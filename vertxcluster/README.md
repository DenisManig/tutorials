# HowTo: create a Vertx Cluster with Webserver and Restservices
This vertx project contains the possibility to run vertx in cluster mode, serve html content from java code, serves static files, like swagger ui and swagger editor and contains example code for a RESTful Webservice from this site: http://vertx.io/blog/some-rest-with-vert-x/

## run in cluster mode
1. create xml file: default-cluster.xml under src/main/resources
2. start verticle with standard Vertx Starter, which accepts -cluster parameter (take default-cluster.xml in same directory ... or under src/main/resources in your IDE like eclipse):

```
> mvn clean compile package
> java -cp target/vertxcluster-0.0.1-SNAPSHOT-fat.jar io.vertx.core.Starter run de.denismanig.tutorial.vertxcluster.WebServerVerticle -cluster
```

run this multiple times at the same machine and for each instance the WebServerVerticle create a Webserver on port 8080. If it could not started, because port 8080 is already bound to the first instance, the verticle increment the port number and try it until it reach 9080. So you get multipe webserver on port 8080, 8081, ... and multiple worker instance with 5 threads per instance. If you browse to any port, one worker from all instances can answer. 

You can test the webserver with html content from java code and workerverticle:

```
[http://localhost:8080](http://localhost:8080)
```

In response you can see which worker from which instance id answers. The port is bound to one instance (first instance on 8080, second to 8081 ...) so your request goes to same instance when you reload the same page, but worker in another instance responses.

## serve static content and do some REST stuff

If you have already a webserver in vertx running, you can serve static content from a folder by adding just one line (example in WebServerVerticle)

```
router.route("/assets/*").handler(StaticHandler.create("assets"));  
```

This serves all files under src/main/resources/assets to the browser. You can call:
 
```
[http://localhost:8080/assets](http://localhost:8080/assets)
```

... and the whisky manager example (vertx with restservices) from [http://vertx.io/blog/some-rest-with-vert-x/](http://vertx.io/blog/some-rest-with-vert-x/) comes up and you can test all restservices, which are implemented in WebServerVerticle.

## swagger static content

you can try swagger by browsing to 

```
[http://localhost:8080/assets/swagger-ui](http://localhost:8080/assets/swagger-ui)
[http://localhost:8080/assets/swagger-editor](http://localhost:8080/assets/swagger-editor)
```

in future versions of this project I want to include the REST documentation from whisky example in swagger, so you can try swagger with real RESTful webservices in this project

## run in eclipse
1. start eclipse and clone this repo in git repositories view
2. import "vertxcluster" project
2. a predefined launcher is in project included: "vertxcluster start". This start this project in cluster mode and take the default-cluster.xml in src/main/resources for cluster config

