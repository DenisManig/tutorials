package de.denismanig.tutorial.vertxcluster;

import java.util.Random;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;

public class SimpleWorkerVerticle extends AbstractVerticle {
	private static int rnd = new Random().nextInt(1000);

	@Override
	public void start() throws Exception {

		vertx.eventBus().consumer("consumer", (Message<String> event) -> {
			if (event.body().equals("1")) {
				event.reply("Denis (Worker Verticle '" + this.deploymentID().substring(0,4) + "' from Instance ID=" + rnd + ")");
			} else {
				event.reply("Berna (Worker Verticle '" + this.deploymentID().substring(0,4) + "' from Instance ID=" + rnd + ")");
			}
		});
	}

}