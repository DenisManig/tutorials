package de.denismanig.tutorial.vertxcluster;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.eventbus.Message;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.Json;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.StaticHandler;

public class WebServerVerticle extends AbstractVerticle {
	Random rand = new Random();
	// Store our product
	private Map<Integer, Whisky> products = new LinkedHashMap<>();
	// Create some product

	@Override
	public void start(Future<Void> fut) {
		DeploymentOptions dpo = new DeploymentOptions().setWorker(true).setInstances(5);
		vertx.deployVerticle(SimpleWorkerVerticle.class.getName(), dpo);
		createSomeData();
		Router router = Router.router(vertx);
		router.route("/").handler(routingContext -> {
			HttpServerResponse response = routingContext.response();
			vertx.eventBus().send("consumer", "" + rand.nextInt(2), (AsyncResult<Message<String>> event) -> answer(event, response));
		});

		// Serve static resources from the /assets directory
		router.route("/assets/*").handler(StaticHandler.create("assets"));

		router.get("/api/whiskies").handler(this::getAll);
		router.route("/api/whiskies*").handler(BodyHandler.create());
		router.post("/api/whiskies").handler(this::addOne);
		router.delete("/api/whiskies/:id").handler(this::deleteOne);
		router.put("/api/whiskies/:id").handler(this::updateOne);
		router.get("/api/whiskies/:id").handler(this::getOne);
		createHttpServer(8080, 9080, router);

	}

	private void createHttpServer(int port, int maxPort, Router router) {
		vertx.createHttpServer().requestHandler(router::accept).listen(config().getInteger("http.port", port), result -> {
			if (result.succeeded()) {
				System.out.println("HTTP Server started on port " + port);
			} else {
				if (port >= maxPort) {
					System.out.println("HTTP Server NOT started on port 8080: " + result.cause());
				} else {
					createHttpServer(port + 1, maxPort, router);
				}

			}
		});
	}

	private void answer(AsyncResult<Message<String>> event, HttpServerResponse response) {
		String answer = null;
		if (event.succeeded()) {
			answer = "<h1>Hello from my first Vert.x 3 application</h1>" + "<h2>" + event.result().body() + "</h2>";
		} else {
			answer = "<h1>Hello from my first Vert.x 3 application</h1>" + "<h2>" + event.cause().getMessage() + "</h2>";
		}
		response.putHeader("content-type", "text/html").end(answer);
	}

	private void addOne(RoutingContext routingContext) {
		final Whisky whisky = Json.decodeValue(routingContext.getBodyAsString(), Whisky.class);
		products.put(whisky.getId(), whisky);
		routingContext.response().setStatusCode(201).putHeader("content-type", "application/json; charset=utf-8").end(Json.encodePrettily(whisky));
	}

	private void getOne(RoutingContext routingContext) {
		String id = routingContext.request().getParam("id");
		if (id == null) {
			routingContext.response().setStatusCode(400).end();
		} else {
			Integer idAsInteger = Integer.valueOf(id);
			Whisky whisky = products.get(idAsInteger);
			routingContext.response().setStatusCode(201).putHeader("content-type", "application/json; charset=utf-8").end(Json.encodePrettily(whisky));
		}
	}

	private void updateOne(RoutingContext routingContext) {
		String id = routingContext.request().getParam("id");
		if (id == null) {
			routingContext.response().setStatusCode(400).end();
		} else {
			Integer idAsInteger = Integer.valueOf(id);
			final Whisky whisky = Json.decodeValue(routingContext.getBodyAsString(), Whisky.class);
			products.put(idAsInteger, whisky);

		}
		routingContext.response().setStatusCode(204).end();
	}

	private void deleteOne(RoutingContext routingContext) {
		String id = routingContext.request().getParam("id");
		if (id == null) {
			routingContext.response().setStatusCode(400).end();
		} else {
			Integer idAsInteger = Integer.valueOf(id);
			products.remove(idAsInteger);
		}
		routingContext.response().setStatusCode(204).end();
	}

	private void createSomeData() {
		Whisky bowmore = new Whisky("Bowmore 15 Years Laimrig", "Scotland, Islay");
		products.put(bowmore.getId(), bowmore);
		Whisky talisker = new Whisky("Talisker 57° North", "Scotland, Island");
		products.put(talisker.getId(), talisker);
	}

	private void getAll(RoutingContext routingContext) {
		routingContext.response().putHeader("content-type", "application/json; charset=utf-8").end(Json.encodePrettily(products.values()));
	}
}